#!/bin/bash

echo "#################################"
echo "   Running OOB_Switch_Config.sh"
echo "#################################"
sudo su

# Configure loopback
cat <<EOT > /etc/network/interfaces

source /etc/network/interfaces.d/*.intf

auto lo
iface lo inet loopback

EOT

cat <<EOT > /etc/network/interfaces.d/vagrant.intf
auto vagrant
iface vagrant inet dhcp

EOT

#/usr/share/doc/ifupdown2/examples/generate_interfaces.py -b | grep -v "#" >> /etc/network/interfaces.d/bridge

#sed -i 's/vagrant//g' /etc/network/interfaces.d/bridge
#sed -i 's/eth0//g' /etc/network/interfaces.d/bridge
#sed -i 's/iface bridge-untagged/iface bridge-untagged inet dhcp/' /etc/network/interfaces.d/bridge

cp /home/vagrant/bridge-untagged /etc/network/interfaces.d/bridge-untagged.intf



echo "#################################"
echo "   Finished "
echo "#################################"

